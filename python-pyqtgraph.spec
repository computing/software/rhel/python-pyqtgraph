%global srcname pyqtgraph
#%global py2_deps PyQt4 numpy python2-pyopengl
%global py2_deps PyQt4 numpy PyOpenGL
%global sum Scientific Graphics and GUI Library for Python
%global desc \
PyQtGraph is a pure-python graphics and GUI library built on PyQt4 / PySide and\
numpy. It is intended for use in mathematics / scientific /engineering \
applications. Despite being written entirely in python, the library is very \
fast due to its heavy leverage of numpy for number crunching and Qt\'s \
GraphicsView framework for fast display.

Name:           python-%{srcname}
Version:        0.10.0
Release:        1.2%{?dist}
Summary:        %{sum}
License:        MIT
URL:            http://www.pyqtgraph.org/
Source0:        http://www.pyqtgraph.org/downloads/%{srcname}-%{version}.tar.gz
#Patch0:         pyqtgraph-0.9.10-disable-failing-tests.patch
#%if 0%{?fedora} < 23
#Patch1:         pyqtgraph-0.9.10-fix-pytest-path.patch
#%endif
#Patch2:         pyqtgraph-0.9.10-testfixes.patch
#Patch3:         pyqtgraph-0.9.10-invalid-slice-fix.patch
#Patch4:         pyqtgraph-0.9.10-index-floats-fix.patch
#Patch5:         pyqtgraph-0.9.10-emit-mouseclickedevent.patch

BuildArch:      noarch
# For Tests
BuildRequires:  python%{python3_pkgversion}-pytest python%{python3_pkgversion}-six
BuildRequires:  xorg-x11-server-Xvfb

%description %{desc}


%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
Requires:       python%{python3_pkgversion}-pyside
Requires:	python%{python3_pkgversion}-numpy
Requires:	python%{python3_pkgversion}-pyside
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:	python%{python3_pkgversion}-numpy
BuildRequires:	python%{python3_pkgversion}-pyside
BuildRequires:	python%{python3_pkgversion}-PyQt4

%description -n python%{python3_pkgversion}-%{srcname} %{desc}

%package doc
Summary:        Documentation for the %{srcname} library

%description doc
This package provides documentation for the %{srcname} library.

%prep
%setup -q -n %{srcname}-%{version}
#%patch5 -p 1
find . -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'

%build
%{__python3} setup.py build

%install
%{__python3} setup.py install -O1 --skip-build --root %{buildroot}
rm -rf %{buildroot}/%{python3_sitelib}/pyqtgraph/examples
rm -f doc/build/html/.buildinfo
rm -f doc/build/html/objects.inv

%check
#xvfb-run -a %{__python2} setup.py test
#pushd %{py3dir}
#xvfb-run -a %{__python3} setup.py test
#popd

%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE.txt
%doc CHANGELOG README.md
%{python3_sitelib}/*

%files doc
%doc examples doc/build/html

%changelog
* Fri Oct 25 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.10.0-1.2
- Drop python3.4, python2 support

* Tue Apr 9 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.10.0-1.1
- Add python36 support

* Mon Jan 29 2018 Magnus Hagdorn <mhagdorn@burn.geos.ed.ac.uk> - 0.9.10-12.3.geos
- rebuild against python34

* Mon Apr  3 2017 Magnus Hagdorn <mhagdorn@burn.geos.ed.ac.uk> - 0.9.10-12.2.geos
- apply patch for wavePicker

* Tue Jan 10 2017 Magnus Hagdorn <mhagdorn@burn.geos.ed.ac.uk> - 0.9.10-12.1.geos
- imported to recipes and build on sl7

* Mon Dec 19 2016 Miro HronÄok <mhroncok@redhat.com> - 0.9.10-12
- Rebuild for Python 3.6

* Wed Jul 20 2016 Scott Talbert <swt@techie.net> - 0.9.10-11
- De-fuzz the disable-failing-tests patch to fix F25 FTBFS

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.10-10
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Tue Mar 01 2016 Scott Talbert <swt@techie.net> - 0.9.10-9
- Update dependency names

* Sat Feb 06 2016 Scott Talbert <swt@techie.net> - 0.9.10-8
- Cherry-pick a couple of upstream patches to fix test failures on F24 rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.10-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Nov 11 2015 Scott Talbert <swt@techie.net> - 0.9.10-6
- Remove pytest path patch on F23+ - fixes FTBFS with Python 3.5

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.10-5
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Fri Aug 07 2015 Scott Talbert <swt@techie.net> - 0.9.10-4
- Moved documentation to subpackage

* Tue Aug 04 2015 Scott Talbert <swt@techie.net> - 0.9.10-3
- Fix and run tests, move examples to docs, add docs

* Sun Aug 02 2015 Scott Talbert <swt@techie.net> - 0.9.10-2
- Build python2 package also; update to latest python packaging standards

* Fri Jul 31 2015 Scott Talbert <swt@techie.net> 0.9.10-1
- Initial packaging.
